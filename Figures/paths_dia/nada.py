import sys
reload(sys)  
sys.setdefaultencoding('utf8') # for plot in PT_BR

import numpy as np

import matplotlib.pyplot as plt
import matplotlib.animation as anim
from matplotlib.ticker import EngFormatter

def plot_graph(route, bestroute):
	fig = plt.figure()
	ax = fig.add_subplot(111)

	# define vertices or nodes as points in 2D cartesian plan
	nsfnodes = [\
		(0.70, 2.70), # 0
		(1.20, 1.70), # 1
		(1.00, 4.00), # 2
		(3.10, 1.00), # 3
		(4.90, 0.70), # 4
		(2.00, 2.74), # 5
		(2.90, 2.66), # 6
		(3.70, 2.80), # 7
		(4.60, 2.80), # 8
		(5.80, 3.10), # 9
		(5.50, 3.90), # 10
		(6.60, 4.60), # 11
		(7.40, 3.30), # 12
		(6.50, 2.40), # 13
	]

	# define links or edges as node index ordered pairs in cartesian plan
	nsflinks = [\
		(0,1), (0,2), (0,5),  # 0
		(1,2), (1,3),         # 1
		(2,8),                # 2
		(3,4), (3,6), (3,13), # 3
		(4,9),                # 4
		(5,6), (5,10),        # 5
		(6,7),                # 6
		(7,8),                # 7
		(8,9),                # 8
		(9,11), (9,12),       # 9
		(10,11), (10,12),     # 10
		(11,13)               # 11
	]

	# grid
	#ax.grid()

	# draw edges before vertices
	for link in nsflinks:
		x = [ nsfnodes[link[0]][0], nsfnodes[link[1]][0] ]
		y = [ nsfnodes[link[0]][1], nsfnodes[link[1]][1] ]
		plt.plot(x, y, 'k--', linewidth=2)

	# highlight in red the shortest path with wavelength(s) available
	if bestroute:
		for i in xrange(len(bestroute)-1):
			x = [ nsfnodes[bestroute[i]][0], nsfnodes[bestroute[i+1]][0] ]
			y = [ nsfnodes[bestroute[i]][1], nsfnodes[bestroute[i+1]][1] ]
			plt.plot(x, y, 'k', linewidth=2.5)

	# draw vertices
	i = 0
	for node in nsfnodes:
		# parameter to adjust text on the center of the vertice
		if i < 10:
			corr = 0.060
		else:
			corr = 0.085

		if i in bestroute:
			plt.plot(node[0], node[1], 'ko', markersize=25, markeredgewidth=3.0)
			ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr), color='white')
		else:
			plt.plot(node[0], node[1], 'wo', markersize=25)
			ax.annotate(str(i), xy=(node[0]-corr, node[1]-corr))

		i += 1
	
	for p in range(5):
		if route == 1:
			plt.text(0.45, 3.15, '$\lambda_1$', rotation=90, color='red', size='x-large', weight='heavy')
			plt.text(0.50, 3.45, '$\lambda_2$', rotation=90, color='#006908', size='x-large', weight='heavy')

			plt.text(1.70, 3.88, '$\lambda_1$', rotation=-10, color='red', size='x-large', weight='heavy')
			plt.text(2.00, 3.78, '$\lambda_2$', rotation=-10, color='#006908', size='x-large', weight='heavy')
			plt.text(2.30, 3.68, '$\lambda_3$', rotation=-10, color='blue', size='x-large', weight='heavy')
			plt.text(2.60, 3.58, '$\lambda_4$', rotation=-10, color='#4b0082', size='x-large', weight='heavy')

			plt.text(4.80, 3.10, '$\lambda_1$', rotation=32, color='red', size='x-large', weight='heavy')
			plt.text(5.10, 3.20, '$\lambda_2$', rotation=32, color='#006908', size='x-large', weight='heavy')

			plt.text(6.55, 2.95, '$\lambda_1$', rotation=20, color='red', size='x-large', weight='heavy')
			plt.text(6.85, 3.00, '$\lambda_3$', rotation=20, color='blue', size='x-large', weight='heavy')
		elif route == 2:
			plt.text(1.25, 2.87, '$\lambda_1$', rotation=15, color='red', size='x-large', weight='heavy')

			plt.text(3.55, 3.55, '$\lambda_1$', rotation=40, color='red', size='x-large', weight='heavy')
			plt.text(3.90, 3.67, '$\lambda_2$', rotation=40, color='#006908', size='x-large', weight='heavy')
			plt.text(4.25, 3.79, '$\lambda_3$', rotation=40, color='blue', size='x-large', weight='heavy')
			plt.text(4.60, 3.91, '$\lambda_4$', rotation=40, color='#4b0082', size='x-large', weight='heavy')

			plt.text(6.57, 3.66, '$\lambda_2$', rotation=-10, color='#006908', size='x-large', weight='heavy')
			plt.text(6.85, 3.55, '$\lambda_3$', rotation=-10, color='blue', size='x-large', weight='heavy')
		elif route == 3:
			plt.text(0.25, 2.05, '$\lambda_1$', rotation=-0, color='red', size='x-large', weight='heavy')
			plt.text(0.55, 2.05, '$\lambda_3$', rotation=-0, color='blue', size='x-large', weight='heavy')

			plt.text(1.50, 1.70, '$\lambda_1$', rotation=-20, color='red', size='x-large', weight='heavy')
			plt.text(1.80, 1.60, '$\lambda_2$', rotation=-20, color='#006908', size='x-large', weight='heavy')
			plt.text(2.10, 1.50, '$\lambda_3$', rotation=-20, color='blue', size='x-large', weight='heavy')
			plt.text(2.40, 1.40, '$\lambda_4$', rotation=-20, color='#4b0082', size='x-large', weight='heavy')

			plt.text(3.75, 1.05, '$\lambda_1$', rotation=-20, color='red', size='x-large', weight='heavy')
			plt.text(4.05, 1.00, '$\lambda_3$', rotation=-20, color='blue', size='x-large', weight='heavy')
			plt.text(4.35, 0.95, '$\lambda_4$', rotation=-20, color='#4b0082', size='x-large', weight='heavy')

			plt.text(5.02, 2.10, '$\lambda_1$', rotation=75, color='red', size='x-large', weight='heavy')
			plt.text(5.12, 2.40, '$\lambda_2$', rotation=75, color='#006908', size='x-large', weight='heavy')
			plt.text(5.22, 2.70, '$\lambda_3$', rotation=75, color='blue', size='x-large', weight='heavy')

			plt.text(6.55, 2.95, '$\lambda_1$', rotation=20, color='red', size='x-large', weight='heavy')
			plt.text(6.85, 3.00, '$\lambda_3$', rotation=20, color='blue', size='x-large', weight='heavy')

	plt.xticks(np.arange(0, 9, 1))
	plt.yticks(np.arange(0, 7, 1))
	plt.show(block=False)

if __name__=='__main__':
	plot_graph(1, [0,2,8,9,12])
	plot_graph(2, [0,5,10,12])
	plot_graph(3, [0,1,3,4,9,12])
	raw_input('Press enter')
