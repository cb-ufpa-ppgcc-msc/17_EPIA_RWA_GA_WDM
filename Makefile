target=llncs

all:
	make clean
	latex $(target).tex
	#bibtex $(target).aux
	#latex $(target).tex
	latex $(target).tex
	dvipdf $(target).dvi
	make clean

clean:
	rm -f *.aux *.dvi *.log *.lof *.out *.toc *.bbl *.blg *.lo
