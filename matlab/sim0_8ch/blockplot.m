hold off;
clear all;
close all;

block_ALT_8;
block_FIX_8;
block_GA_8;
block_STA_8;
block_STF_8;

%figure('units','normalized','outerposition',[0 0 1 1]);
figure;
h1 = plot(mean(FIX), 'k+--',  'linewidth', 2, 'markersize', 20, 'markerfacecolor', 'w'); % Dijkstra + First Fit
hold on;
h2 = plot(mean(STF), 'm^--',   'linewidth', 2, 'markerfacecolor', 'm'); % Dijkstra + Graph Coloring
hold on;

h3 = plot(mean(GA),  'o--',  'linewidth', 2, 'markersize', 18, 'color', [0 0.4 0.99]); % GA + GOF
hold on;
h4 = plot(mean(STA), 's--', 'linewidth', 2, 'markersize', 12, 'markerfacecolor', 'w', 'color', [0 0.5 0]); % Yen + Graph Coloring
hold on;
h5 = plot(mean(ALT), 'r*--',  'linewidth', 1, 'markerfacecolor', 'r'); % Yen + First Fit
hold on;
grid on;

h = legend([h1 h2 h3 h4 h5], ...
' Dijkstra + First-Fit',... % k
' Dijkstra + Graph Coloring',... % g
' Genetic Algorithm + GOF',... % b
' Yen + Graph Coloring',... % c
' Yen + First-Fit',... % r
'location', 'northwest');
set(h, 'fontsize', 16);

xlim([0.8 20.2]);
ylim([-2.0 70.0]);
set(gca, 'XTick', 2:2:20, 'fontsize', 20);
set(gca, 'YTick', 0:10:70, 'fontsize', 20);
grid on;

xlabel('Load (Erlangs)', 'fontsize', 24);
ylabel('Blocking Probability (%)', 'fontsize', 24);
title('8 channels, Yen K = 2 alternate routes');

%set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.

%%% EOF %%%
