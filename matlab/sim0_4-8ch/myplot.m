clear all;
hold off;
close all;

figure;
case_1; plot(1:20, mean(STD), 'ro--', 'LineWidth', 2, 'MarkerSize', 12);
hold on;
case_3; plot(1:20, mean(STD), 'm^:',  'LineWidth', 2, 'MarkerSize', 12, 'MarkerFaceColor', 'm');
hold on;
case_1; plot(1:20, mean(GA),  'bh--', 'LineWidth', 2, 'MarkerSize', 12);
hold on;
case_3; plot(1:20, mean(GA),  'kd:',  'LineWidth', 2, 'MarkerSize', 12, 'MarkerFaceColor', 'k');
hold on;

h = legend(...
' Dijkstra + Graph Coloring (4 channel net)',...
' Dijkstra + Graph Coloring (8 channel net)',...
' Genetic Algorithm + GOF (4 channel net)',...
' Genetic Algorithm + GOF (8 channel net)',...
'Location', 'NorthWest');
set(h, 'fontsize', 24);

xlim([0.8 20.2]);
ylim([-2.0 100.0]);
set(gca, 'XTick', 1:20, 'fontsize', 24);
grid on;

xlabel('Load (Erlangs)', 'fontsize', 40);
ylabel('Blocking Probability (%)', 'fontsize', 40);
